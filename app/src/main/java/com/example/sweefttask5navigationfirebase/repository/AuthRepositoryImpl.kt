package com.example.sweefttask5navigationfirebase.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor() : AuthRepository {

    private val auth = Firebase.auth

    override fun logInUser(email:String, password:String) : Task<AuthResult> {
        return auth.signInWithEmailAndPassword(email, password)
    }

    override fun registerUser(email: String, password: String): Task<AuthResult> {
        return auth.createUserWithEmailAndPassword(email, password)
    }

    override fun signOut() {
        auth.signOut()
    }

}