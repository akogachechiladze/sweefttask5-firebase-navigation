package com.example.sweefttask5navigationfirebase.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult

interface AuthRepository {

    fun logInUser(email:String, password:String) : Task<AuthResult>

    fun registerUser(email: String, password: String): Task<AuthResult>

    fun signOut()

}