package com.example.sweefttask5navigationfirebase.di

import com.example.sweefttask5navigationfirebase.repository.AuthRepository
import com.example.sweefttask5navigationfirebase.repository.AuthRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface AppModule {

    @Binds
    @Singleton
    fun bindAuthRepository(authRepositoryImpl: AuthRepositoryImpl) : AuthRepository

}