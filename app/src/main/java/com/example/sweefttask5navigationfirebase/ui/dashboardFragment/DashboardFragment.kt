package com.example.sweefttask5navigationfirebase.ui.dashboardFragment

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.sweefttask5navigationfirebase.databinding.DashboardFragmentBinding
import com.example.sweefttask5navigationfirebase.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : BaseFragment<DashboardFragmentBinding, DashboardViewModel>(DashboardFragmentBinding:: inflate) {

    override val viewModelClass: Class<DashboardViewModel>
        get() = DashboardViewModel::class.java

    private val args: DashboardFragmentArgs by navArgs()

    override fun setUp() {
        getArgs()
        initListener()
    }

    private fun initListener() {
        binding.logOutButton.setOnClickListener {
            signOut()
            val action = DashboardFragmentDirections.actionDashboardFragmentToLogInFragment()
            findNavController().navigate(action)
        }
    }

    private fun getArgs() {
        binding.emailTV.text = args.email
    }

    private fun signOut() {
        viewModel.signOut()
    }

}