package com.example.sweefttask5navigationfirebase.ui.logInFragment

import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sweefttask5navigationfirebase.ui.AuthStates
import com.example.sweefttask5navigationfirebase.databinding.LogInFragmentBinding
import com.example.sweefttask5navigationfirebase.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LogInFragment : BaseFragment<LogInFragmentBinding, LogInViewModel>(LogInFragmentBinding:: inflate) {

    override val viewModelClass: Class<LogInViewModel>
        get() = LogInViewModel::class.java

    override fun setUp() {
        getResult()
        initListener()
    }

    private fun initListener() {
        with(binding) {
            registerButton.setOnClickListener {
                val action = LogInFragmentDirections.actionLogInFragmentToRegisterFragment()
                findNavController().navigate(action)
            }

            logInButton.setOnClickListener {
                val email = emailET.text.toString()
                val password = passwordET.text.toString()
                if (email.isNotEmpty() && password.isNotEmpty()) {
                    logIn(email, password)
                }else {
                    toastMessage(FILL_FIELDS)
                }

            }
        }
    }

    private fun getResult() {
        with(binding) {
            setFragmentResultListener(REQ_K_1) { _, bundle ->
                val result = bundle.getString(BUNDLE_K_1)
                if (result != null) {
                    emailET.setText(result)
                }else
                    emailET.setText("")
            }
            setFragmentResultListener(REQ_K_2) { _, bundle ->
                val result = bundle.getString(BUNDLE_K_2)
                if (result != null) {
                    passwordET.setText(result)
                }else
                    passwordET.setText("")
            }
        }
    }

    private fun logIn(email: String, password: String) {
        with(binding) {
            viewModel.logInUser(email, password)
            viewModel.authStateFlow.observe(viewLifecycleOwner) {
                when (it) {
                    is AuthStates.AuthSuccess -> {progressBar.visibility = View.GONE
                        val action = LogInFragmentDirections.actionLogInFragmentToDashboardFragment(email)
                        findNavController().navigate(action)}
                    is AuthStates.Loading -> progressBar.visibility = View.VISIBLE
                    is AuthStates.Error -> {progressBar.visibility = View.GONE
                        toastMessage(it.message!!)}
                    is AuthStates.Idle -> progressBar.visibility = View.GONE
                    else -> {toastMessage(UNKNOWN_ERROR)}
                }
            }
        }
    }

}