package com.example.sweefttask5navigationfirebase.ui.registerFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sweefttask5navigationfirebase.repository.AuthRepository
import com.example.sweefttask5navigationfirebase.ui.AuthStates
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {

    private val _authStateFlow = MutableLiveData<AuthStates>(AuthStates.Idle)
    val authStateFlow: LiveData<AuthStates> get() = _authStateFlow

    fun registerUser(email: String, password: String) {
        _authStateFlow.postValue(AuthStates.Loading)
        authRepository.registerUser(email, password).addOnSuccessListener {
            _authStateFlow.postValue(AuthStates.AuthSuccess())
        }.addOnFailureListener{
            _authStateFlow.postValue(AuthStates.Error(it.message.toString()))
        }
    }

}