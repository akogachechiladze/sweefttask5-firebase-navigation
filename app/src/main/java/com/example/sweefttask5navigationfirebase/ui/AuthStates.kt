package com.example.sweefttask5navigationfirebase.ui

import com.google.firebase.auth.FirebaseUser

sealed interface AuthStates {
    data class AuthSuccess(val user: FirebaseUser? = null) : AuthStates
    data class Error(val message: String?) : AuthStates
    object Loading: AuthStates
    object Idle: AuthStates
}