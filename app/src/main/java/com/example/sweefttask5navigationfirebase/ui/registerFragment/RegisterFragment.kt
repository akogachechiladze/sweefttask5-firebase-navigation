package com.example.sweefttask5navigationfirebase.ui.registerFragment

import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sweefttask5navigationfirebase.ui.AuthStates
import com.example.sweefttask5navigationfirebase.databinding.RegisterFragmentBinding
import com.example.sweefttask5navigationfirebase.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : BaseFragment<RegisterFragmentBinding, RegisterViewModel>(RegisterFragmentBinding:: inflate) {

    override val viewModelClass: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java

    override fun setUp() {
        initListeners()
    }

    private fun initListeners() {
        with(binding) {
            registerButton.setOnClickListener {
                val email = emailET.text.toString()
                val password = passwordET.text.toString()
                val repeatPassword = repeatPasswordET.text.toString()
                if (email.isNotEmpty() && password.isNotEmpty() && password == repeatPassword) {
                    registerUser(email, password)
                }else {
                    toastMessage(FILL_FIELDS)
                }
            }
        }
    }

    private fun registerUser(email: String, password: String) {
        with(binding) {
            viewModel.registerUser(email, password)
            viewModel.authStateFlow.observe(viewLifecycleOwner){
                when (it) {
                    is AuthStates.AuthSuccess -> {progressBar.visibility = View.GONE
                        val action = RegisterFragmentDirections.actionRegisterFragmentToLogInFragment()
                        findNavController().navigate(action)
                        setFragmentResult(REQ_K_1, bundleOf(BUNDLE_K_1 to email))
                        setFragmentResult(REQ_K_2, bundleOf(BUNDLE_K_2 to password))}
                    is AuthStates.Loading -> {progressBar.visibility = View.VISIBLE}
                    is AuthStates.Error -> {progressBar.visibility = View.VISIBLE
                        toastMessage(it.message!!)}
                    is AuthStates.Idle -> progressBar.visibility = View.GONE
                    else -> {toastMessage(UNKNOWN_ERROR)}
                }
            }
        }
    }



}