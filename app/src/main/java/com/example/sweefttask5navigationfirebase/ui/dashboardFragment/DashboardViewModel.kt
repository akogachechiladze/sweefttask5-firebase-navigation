package com.example.sweefttask5navigationfirebase.ui.dashboardFragment

import androidx.lifecycle.ViewModel
import com.example.sweefttask5navigationfirebase.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {

    fun signOut() {
        authRepository.signOut()
    }

}