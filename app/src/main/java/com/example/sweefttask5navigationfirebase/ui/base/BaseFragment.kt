package com.example.sweefttask5navigationfirebase.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.viewbinding.ViewBinding

typealias Inflate<T> = (LayoutInflater, ViewGroup, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding, VM: ViewModel>(private val inflate: Inflate<VB>): Fragment() {

    abstract val viewModelClass : Class<VM>
    private var _binding: VB? = null
    val binding get() = _binding!!
    protected lateinit var viewModel : VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[viewModelClass]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        return binding.root
    }

    abstract fun setUp()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected fun toastMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val FILL_FIELDS = "Please Fill All Fields"
        const val UNKNOWN_ERROR = "Unknown Error"
        const val REQ_K_1 = "requestKey1"
        const val REQ_K_2 = "requestKey2"
        const val BUNDLE_K_1 = "bundleKey1"
        const val BUNDLE_K_2 = "bundleKey2"

    }

}