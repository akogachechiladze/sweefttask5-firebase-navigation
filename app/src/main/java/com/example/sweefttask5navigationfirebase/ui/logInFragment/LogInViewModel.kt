package com.example.sweefttask5navigationfirebase.ui.logInFragment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sweefttask5navigationfirebase.repository.AuthRepository
import com.example.sweefttask5navigationfirebase.ui.AuthStates
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LogInViewModel @Inject constructor(private val authRepository: AuthRepository): ViewModel() {

    private val _authStateFlow = MutableLiveData<AuthStates>(AuthStates.Idle)
    val authStateFlow: LiveData<AuthStates> get() = _authStateFlow

    fun logInUser(email: String,  password: String) {
        _authStateFlow.postValue(AuthStates.Loading)
        authRepository.logInUser(email, password).addOnSuccessListener {
            val user = it.user
            Log.d("12345", "signInWithEmail:success")
            _authStateFlow.postValue(AuthStates.AuthSuccess(user))
        }.addOnFailureListener {
            _authStateFlow.postValue(AuthStates.Error(it.message.toString()))
        }
    }
}